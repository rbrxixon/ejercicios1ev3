package unidad8.colecciones;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Ejercicio7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		String entrada;
		String[] cadena;
		int n;
		int m;
		Set<Integer> arrSet1 = new HashSet<Integer>();
		Set<Integer> arrSet2 = new HashSet<Integer>();
		
        try {
    		System.out.println("Indica cantidades N y M: ");
    		entrada = reader.readLine();
    		cadena = entrada.split(" ");
    		n = Integer.parseInt(cadena[0]);
    		m = Integer.parseInt(cadena[1]);
    		    		
    		System.out.println("Indica elementos de ambos conjuntos separados por espacios en blanco: ");
    		entrada = reader.readLine();
    		cadena = entrada.split(" ");
  		    			
    		for (int i = 0; i < n; i++) {
				arrSet1.add(Integer.parseInt(cadena[i]));
			}
    		
    		for (int i = n; i < n+m; i++) {
				arrSet2.add(Integer.parseInt(cadena[i]));				
			}
    		
    		arrSet1.retainAll(arrSet2);
    		System.out.println(arrSet1);
    			
        } catch (Exception e) {
			// TODO: handle exception
        	System.out.println(e);
		}
	}
}
