package unidad8.colecciones;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Ejercicio2 {
	List<Integer> lista = new ArrayList<Integer>();
	List<Integer> lista2 = new ArrayList<Integer>();


	private void punto1() {
		// Almacenar en una lista 100 números aleatorios entre 1 y 100 y mostrarlos todos utilizando uno de los métodos de iteración. 
		System.out.println("# Punto 1 #");
		for (int i = 0; i < 100; i++) {
			lista.add(new Random().nextInt(100) + 1);	
			System.out.println(lista.get(i));
		}
	}
	
	private boolean comprobar(int numero) {
		for (int j = 0; j < lista2.size(); j++) {
			if (numero == lista2.get(j)) {
				//System.out.println("Igual. Entrada: " + numero + " Salida: " + lista2.get(j));
				return false;
			}
		}
		return true;
	}
	
	private void punto2() {
		// Almacenar los números de la lista original en otra colección sin que se repita ninguno y mostrarlos todos usando un método de iteración diferente al anterior.
		System.out.println("# Punto 2 #");
    	for (Integer numero : lista) {
    		if (comprobar(numero)) {
    			lista2.add(numero);
    			//System.out.println(numero);
    		}
		}
    	
    	for (Integer numero : lista2) {
			System.out.println(numero);
    	}
	}
	
	private void punto3() {
		// Almacenar los números de la lista original en otra colecciónen la que se almacenen ordenados y sin que se repita ninguno y mostrarlos todos usando un método de iteración diferente al anterior.
		System.out.println("# Punto 3 #");
		Collections.sort(lista2);
		for (Iterator iterator = lista2.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.println(integer);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ejercicio2 e = new Ejercicio2();
		e.punto1();
		e.punto2();
		e.punto3();
	}

}
