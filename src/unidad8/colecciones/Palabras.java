package unidad8.colecciones;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

public class Palabras {
	SortedMap<String, Integer> listaPalabras;
               
	public Palabras() {
		listaPalabras = new TreeMap<String, Integer>();
		System.out.println("Palabra map creado vacio");
	}
    	
	public Palabras(String palabra) {
		System.out.println("Palabra map creado vacio con palabra '" + palabra + "'");
		listaPalabras = new TreeMap<String, Integer>();
		List<String> cadena = new ArrayList<String>(Arrays.asList(palabra.split(" ")));
		
		insertarPalabras(cadena);
	}
	
	private void insertarPalabras(List<String> cadena) {
		for (String item : cadena) {
			getListaPalabras().put(item, item.length());
		}
		System.out.println("Lista palabras actual:");
		getListaPalabras().entrySet().forEach(System.out::println);
	}
	
	private SortedMap<String, Integer> getListaPalabras() {
		return listaPalabras;
	}

	private void setListaPalabras(SortedMap<String, Integer> listaPalabras) {
		this.listaPalabras = listaPalabras;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Palabras p1 = new Palabras();
		Palabras p = new Palabras("Erase una vez el cuerpo humano y una gata a la que el cuerpo no le gustaba");
		Scanner sc = new Scanner(System.in);
		String entrada;
		//String[] cadena;
		boolean seguir = true;
		//Palabras p = new Palabras();
		List<String> cadena;
		
		do {
			System.out.print("> ");
			entrada = sc.nextLine();
			cadena = new ArrayList<String>(Arrays.asList(entrada.split(" ")));
			int n;
			
			try {
				switch (cadena.get(0).toLowerCase()) {
				case "añadir:":
					System.out.println("Se almacenarán las palabras escritas a continuación de los dos puntos");
					cadena.remove(0);
					p.insertarPalabras(cadena);
					break;
				case "lista":
					try {
						n = Integer.parseInt(cadena.get(1));
						System.out.println("Se mostrará la lista de palabras de longitud: " + n);
						
						for(Map.Entry<String,Integer> entry : p.getListaPalabras().entrySet()) {
							if (entry.getValue() == n) {
								System.out.println(entry.getKey());
							}
						}
						
					} catch (NumberFormatException e) {
						// TODO: handle exception
						System.out.println(e);
					}
					break;
				case "borrar":
						System.out.println("Borrar todas las palabras almacenadas");
						p.getListaPalabras().clear();
					break;
				case "borrar:":
					System.out.println("Sustituye las palabras almacenadas anteriormente por las palabras escritas a continuación de los dos puntos");
					p.getListaPalabras().clear();
					cadena.remove(0);
					p.insertarPalabras(cadena);
					break;
	
				case "fin":
					System.out.println("Borra todas las palabras almacenadas");
					p.getListaPalabras().clear();
					seguir = false;
					break;
					
				default:
					System.out.println("Comando desconocido");
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
		}
		while (seguir);
			
			
		
		
	}

}
