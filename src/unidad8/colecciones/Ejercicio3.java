package unidad8.colecciones;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Ejercicio3 {
	static ArrayList<String> lista;
	static ArrayList<String> listaRepetidas;
	static ArrayList<String> listaNoRepetidas;

	private static boolean comprobar(String cadena) {
		for (int j = 0; j < listaNoRepetidas.size(); j++) {
			if (cadena.equals(listaNoRepetidas.get(j))) {
				//System.out.println("Igual. Entrada: " + cadena + " Salida: " + listaNoRepetidas.get(j));
				return false;
			} 
		}
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	       BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	       String entrada = null; 
	       int numeroPalabras = 0;
	       lista = new ArrayList<String>();
	       listaRepetidas = new ArrayList<String>();
	       listaNoRepetidas = new ArrayList<String>();
	        
	        try {
        		System.out.print("Introduce numero de palabras: ");
        		entrada = reader.readLine();
	        	numeroPalabras = Integer.parseInt(entrada);
	        	
	        	for (int i = 0; i < numeroPalabras; i++) {
	        		System.out.print("Introduce palabra #" + (i+1) + ": ");
	        		entrada = reader.readLine();
	        		lista.add(entrada);
				}
	        	
	        	for (int i = 0; i < lista.size(); i++) {
					if (comprobar(lista.get(i))) {
						System.out.println("Listado no repetidas");
						listaNoRepetidas.add(lista.get(i));
					} else {
						System.out.println("Listado repetidas");
						listaRepetidas.add(lista.get(i));
					}
				}
	        	
	        	System.out.println("listaNoRepetidas");
	        	for (String string : listaNoRepetidas) {
					System.out.println(string);
				}
	        	
	        	System.out.println("listaRepetidas");
	        	for (String string : listaRepetidas) {
					System.out.println(string);
				}
	        	
	        	
	        } catch (Exception e) {
				// TODO: handle exception
	        	System.out.println(e);
			}
	}

}
