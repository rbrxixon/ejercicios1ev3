package unidad8.colecciones;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

public class Colecciones {

	private static void eliminarLasLongitudesPar(Set<String> cadena) {
		System.out.println("### Ejercicio 1");
		Iterator<String> i = cadena.iterator();
		while (i.hasNext()) { 
			if (i.next().length() == 2)
				i.remove();
		}
		
		System.out.println("Cadena final:");
		Iterator<String> x = cadena.iterator();
		while (x.hasNext()) {
			System.out.println(x.next());
		}
	}
	
	private static boolean contieneImpares(Set<Integer> cadena) {
		System.out.println("### Ejercicio 2");
		Iterator<Integer> x = cadena.iterator();
		while (x.hasNext()) {
			if ((x.next()%2!=0))
				return true;
		}
		return false;
	}
	
	private static boolean valoresUnicos(Map<String, String> mapa) {
		System.out.println("### Ejercicio 3");
		int contador = 0;
		boolean result = false;
		for(Entry<String, String> entry : mapa.entrySet()) {
			contador = 0;
			String palabra = entry.getValue();
			for(Entry<String, String> entry2 : mapa.entrySet()) {
				if (palabra.equals(entry2.getValue())) {
					contador++;
				}
				//System.out.println("palabra " + palabra + " contador " + contador);
			}
			if (contador > 1) {
				result = true;
				break;
			} else
				result = false;
		}
		return result;
	}
	
	private static boolean algunaSeRepiteAlMenos3Veces(List<String> cadena) {
		System.out.println("### Ejercicio 4");
		Map<String, Integer> mapa = new HashMap<String,Integer>();
		for (String item : cadena) {
			if (mapa.containsKey(item)) {
				//System.out.println("Existe " + item);
				mapa.put(item, mapa.get(item) + 1);
			} else {
				//System.out.println("No Existe " + item);
				mapa.put(item, 1);
			}
		}
		
		for (Entry<String, Integer> e: mapa.entrySet()) {
			System.out.println(e.getKey() + " " + e.getValue());
			if (e.getValue() > 2) {
				return true;
			}
		}
		
		return false;
	}
	
	private static void negativosAbajoPositivosArriba(PriorityQueue<Integer> pila) {
		System.out.println("### Ejercicio 5");
	
		while(!pila.isEmpty()) {
			System.out.println(pila.poll());
		}
	}
	
	private static int moda(List<Integer> lista) {
		System.out.println("### Ejercicio 6");
		int moda = 0;
		Map<Integer, Integer> mapa = new HashMap<Integer,Integer>();
		for (Integer item : lista) {
			if (mapa.containsKey(item)) {
				mapa.put(item, mapa.get(item) + 1);
			} else {
				mapa.put(item, 1);
			}

			if (moda <= mapa.get(item))
				moda = mapa.get(item);
		}
		return moda;
	}
	
	private static void eliminarSiMayoresEncima(Deque<Integer> pilaNumeros1) {
		System.out.println("### Ejercicio 7");
		
		Deque<Integer> pilaNumeros2 = new LinkedList<Integer>(pilaNumeros1);
		Stack<Integer> pilaNumerosFinal = new Stack<Integer>();
		
		int posicion1 = 0;
		int posicion2 = 0;
		Iterator<Integer> itNum1 = pilaNumeros1.iterator();
		Iterator<Integer> itNum2 = pilaNumeros2.iterator();
		int num1 = 0;
		int num2 = 0;
		boolean copiar = true;
		
		while (itNum1.hasNext()) {
			posicion2 = 0;
			copiar = true;
			pilaNumeros2 = pilaNumeros1;
			itNum2 = pilaNumeros2.iterator();
			num1 = itNum1.next();
			while (itNum2.hasNext() && (posicion1 >= posicion2)) {
				num2 = itNum2.next();
				//System.out.println("# Posicion1 " + posicion1 + " Posicion2 " + posicion2);
				//System.out.println("Num1: " + num1 + " | Num2: " + num2);
				if ( num1 < num2) {
					//System.out.println("Mayor, eliminar");
					copiar = false;
					break;
				} else {
					//System.out.println("Menor o igual, no hacer nada");
				}
				posicion2++;
			}
			if (copiar) {
				//System.out.println("!! Añadir a colaFinal " + num1);
				pilaNumerosFinal.add(num1);
			}
			posicion1++;
		}
		System.out.println("Impresion final");	
		System.out.println(pilaNumerosFinal.toString());
	}
	
	private static void interseccion(Map<String, Integer> mapaInterseccion1, Map<String, Integer> mapaInterseccion2) {
		System.out.println("### Ejercicio 8");
		Map<String,Integer> mapaInterseccionFinal = new HashMap<>(mapaInterseccion1);
		mapaInterseccionFinal.keySet().retainAll(mapaInterseccion2.keySet());
		System.out.println(mapaInterseccionFinal);
	}
	
	private static void valorMenosRepetido(Map<String, Integer> mapaEdades) {
		System.out.println("### Ejercicio 9");
		Map<Integer,Integer> mapaContadorEdades = new TreeMap<>();
		Integer numero1;
		Integer numero2;
		Integer cantidad;
		List<Integer> listadoMinimo = new ArrayList<Integer>();
		
		for(Entry<String, Integer> entry : mapaEdades.entrySet()) {
			numero1 = entry.getValue();
			for(Entry<String, Integer> entry2 : mapaEdades.entrySet()) {
				numero2 = entry2.getValue();
				if (numero1 == numero2) {
					if (mapaContadorEdades.containsKey(numero2)) {
						cantidad = mapaContadorEdades.get(numero2);
						//System.out.println("= Numero 2 " + numero2 + " Cantidad " + cantidad);
						mapaContadorEdades.put(numero2, cantidad + 1);
						break;
					} else {
						mapaContadorEdades.put(numero2, 1);
						//System.out.println("+ Numero 2 " + numero2 + " Cantidad " + 1);
						break;
					}
				} 
			}
		}
		//System.out.println(mapaContadorEdades);
		
		int minimo = 0;
		for(Entry<Integer, Integer> entry3 : mapaContadorEdades.entrySet()) {
			if (minimo == 0) {
				minimo = entry3.getValue();
			}

			if (minimo > entry3.getValue()) {
				minimo = entry3.getValue();
			} 
		}
		//System.out.println("Minimo " + minimo);
		
		for(Entry<Integer, Integer> entry4 : mapaContadorEdades.entrySet()) {
			if (entry4.getValue() == minimo) {
				listadoMinimo.add(entry4.getKey());
			}
		}
		
		//System.out.println("numero minimo");
		int numeroMenosRepetido = 0;
		for (Integer numeroMinimo : listadoMinimo) {
			//System.out.println(numeroMinimo);
			if (numeroMenosRepetido == 0) {
				numeroMenosRepetido = numeroMinimo;
			}
			
			if (numeroMenosRepetido > numeroMinimo) {
				numeroMenosRepetido = numeroMinimo;
			}
		}
		System.out.println(numeroMenosRepetido);

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		// Ejercicio 1
		Set<String> conjuntoCadenas = new TreeSet<String>();
		conjuntoCadenas.add("Uno");
		conjuntoCadenas.add("Dos");
		conjuntoCadenas.add("El");
		conjuntoCadenas.add("Otro");
		conjuntoCadenas.add("No");
		eliminarLasLongitudesPar(conjuntoCadenas);
		// Ejercicio 2		
		Set<Integer> conjuntoNumeros = new TreeSet<Integer>();
		conjuntoNumeros.add(2);
		conjuntoNumeros.add(2);
		conjuntoNumeros.add(3);
		conjuntoNumeros.add(6);
		System.out.println(contieneImpares(conjuntoNumeros));
		// Ejercicio 3
		Map<String, String> mapaCadenaCadena = new TreeMap<String, String>();
		mapaCadenaCadena.put("1", "Uno");
		mapaCadenaCadena.put("2", "Uno");
		mapaCadenaCadena.put("3", "Dos");
		mapaCadenaCadena.put("4", "Tres");
		mapaCadenaCadena.put("5", "Cuatro");
		System.out.println(valoresUnicos(mapaCadenaCadena));
		// Ejercicio 4
		List<String> listaCadena = new ArrayList<String>();
		listaCadena.add("Uno");
		listaCadena.add("Uno");
		listaCadena.add("Uno");
		listaCadena.add("Dos");
		listaCadena.add("Tres");
		System.out.println(algunaSeRepiteAlMenos3Veces(listaCadena));
		// Ejercicio 5
		   Comparator<Integer> comparador = new Comparator<Integer>() {
	       @Override
		       	public int compare(Integer a, Integer b) {
		    	   int result = 0;
		    	   if (a >= 0)
		    		   result = -1;
		    	   if (a < 0)
		    		   result = 1;
		    	   return result;
		        }
	        };
		PriorityQueue<Integer> pila = new PriorityQueue<Integer>(comparador);
		pila.add(89);
		pila.add(-55);
		pila.add(-19);
		pila.add(29);
		pila.add(37);
		pila.add(-5);
		pila.add(23);
		negativosAbajoPositivosArriba(pila);
		// Ejercicio 6
		List<Integer> listaNumerosModa = new ArrayList<Integer>();
		listaNumerosModa.add(2);
		listaNumerosModa.add(2);
		listaNumerosModa.add(3);
		listaNumerosModa.add(3);
		listaNumerosModa.add(3);
		listaNumerosModa.add(4);
		listaNumerosModa.add(5);
		System.out.println(moda(listaNumerosModa));
		
		// Ejercicio 7
		Deque<Integer> listaPilaMayores = new LinkedList<Integer>();
		listaPilaMayores.add(2);
		listaPilaMayores.add(7);
		listaPilaMayores.add(12);
		listaPilaMayores.add(5);
		listaPilaMayores.add(14);
		listaPilaMayores.add(9);
		listaPilaMayores.add(7);
		listaPilaMayores.add(10);
		listaPilaMayores.add(17);
		listaPilaMayores.add(17);
		listaPilaMayores.add(22);
		listaPilaMayores.add(6);
		eliminarSiMayoresEncima(listaPilaMayores);
		
		// Ejercicio 8
		Map<String,Integer> mapaInterseccion1 = new HashMap<String,Integer>();
		Map<String,Integer> mapaInterseccion2 = new HashMap<String,Integer>();
		mapaInterseccion1.put("Fernando", 53);
		mapaInterseccion1.put("Manuela", 29);
		mapaInterseccion1.put("Ana", 41);
		mapaInterseccion1.put("Luis", 65);
		mapaInterseccion1.put("Mario", 33);
		mapaInterseccion1.put("Adrian", 21);
		mapaInterseccion1.put("Carmen", 39);
		mapaInterseccion1.put("Elena", 19);
		mapaInterseccion2.put("Valentina", 53);
		mapaInterseccion2.put("Ana", 53);
		mapaInterseccion2.put("Mario", 53);
		mapaInterseccion2.put("Benito", 53);
		mapaInterseccion2.put("Carmen", 53);
		mapaInterseccion2.put("Ramon", 53);
		mapaInterseccion2.put("Elena", 53);
		mapaInterseccion2.put("Hugo", 53);
		interseccion(mapaInterseccion1, mapaInterseccion2);
		// Ejercicio 9
		Map<String,Integer> mapaEdades = new TreeMap<String,Integer>();
		mapaEdades.put("Juan", 33);
		mapaEdades.put("Hugo", 29);
		mapaEdades.put("Ana", 45);
		mapaEdades.put("Luis", 47);
		mapaEdades.put("Mario", 33);
		mapaEdades.put("Rosa", 29);
		mapaEdades.put("Carmen", 33);
		mapaEdades.put("Elena", 59);
		mapaEdades.put("Benito", 33);
		// 33=4  29=2 45=1 47=1 59=1
		valorMenosRepetido(mapaEdades);
		*/
		// Ejercicio 10
		
		
	}





}

