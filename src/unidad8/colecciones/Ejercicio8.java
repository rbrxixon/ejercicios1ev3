package unidad8.colecciones;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

public class Ejercicio8 {
	
	HashMap<String, HashSet<String>> listado = new HashMap<String, HashSet<String>>();
	
	
	private void procesarLinea(String entrada) {
		String[] cadenaEntrada = entrada.trim().split(":");
		String jugador = cadenaEntrada[0];
		String[] cadenaNaipe = cadenaEntrada[1].trim().split(" ");
		
		if (listado.containsKey(jugador)) {
			//System.out.println("Jugador previamente creado");
			HashSet<String> naipes = listado.get(jugador);
			for (String naipe : cadenaNaipe) {
	    		//System.out.println("Naipe: " + naipe);
	    		naipes.add(naipe);
			}
		} else {
			//System.out.println("Jugador nuevo");
			
			HashSet<String> naipes = new HashSet<String>();
			for (String naipe : cadenaNaipe) {
	    		//System.out.println("Naipe: " + naipe);
	    		naipes.add(naipe);
			}
			listado.put(jugador, naipes);
		}
	}
	
	private void listarCartasJugadores() {
		//System.out.println("listarCartasJugadores");
		HashSet<String> naipesSinRepetir = null;
		Iterator<Entry<String, HashSet<String>>> i = listado.entrySet().iterator();
		while(i.hasNext()) {
			Entry<String, HashSet<String>> e = i.next();
			//System.out.println(e.getValue().retainAll(e.getValue()));
			String jugador = e.getKey();
			//System.out.println("Jugador " + jugador);
			naipesSinRepetir = new HashSet<String>();
			naipesSinRepetir = e.getValue();
			naipesSinRepetir.retainAll(e.getValue());
			
			/*
			for (String naipe : naipesSinRepetir) {
	    		System.out.println(naipe);
			} 
			*/
			calcularPuntuacion(jugador, naipesSinRepetir);
		}
		
	}
	
	private void calcularPuntuacion(String jugador, HashSet<String> naipes) {
		//System.out.println("calcularPuntuacion");
		//System.out.println("Jugador " + jugador);
		int puntuacionTotal = 0;
		for (String naipe : naipes) {
			puntuacionTotal += calcularValor(naipe);
		} 	
		System.out.println(jugador + ": " + puntuacionTotal);
	}
	
	private int calcularValor(String naipe) {
		//System.out.println("calcularValor");
		String[] naipeSeparado;
		//System.out.println("Naipe: " + naipe);
		naipeSeparado = naipe.split("S|H|D|C");
		String rango = naipeSeparado[0];
		String palo = naipe.replace(rango, "");
		int rangoValor = 0;
		int puntuacion = 0;
		int paloValor = 0;
		
		
		switch (rango) {
			case "J":
				rangoValor = 11;
				break;
			case "Q":
				rangoValor = 12;
				break;
			case "K":
				rangoValor = 13;
				break;
			case "A":
				rangoValor = 14;
				break;
			default:
				rangoValor = Integer.parseInt(rango);
				break;
		}
		
		switch (palo) {
			case "S":
				paloValor = 4;
				break;
			case "H":
				paloValor = 3;
				break;
			case "D":
				paloValor = 2;
				break;
			case "C":
				paloValor = 1;
				break;
		}
		
		puntuacion = (rangoValor * paloValor);
		//System.out.println("Naipe: " + naipe + " Rango " + rango + " palo " + palo + " Puntuacion: " + puntuacion);
		return puntuacion;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Leer consola continuo, while boolean con seguir
		// Switch par leer FIN y terminar con seguir false
		// Separar en cadena hasta los ':' 
		// Solo puede existir un ':' que hara de separador
		// Crear un set si no existe la key (nombre), sino añadir al mismo set
		// Despues de separar el nombre, coger la otra parte y separarla por espacios
		// Crear switch para los valores especiales j, Q, k, A
		// Definir variables para el valor de cada naipe
		// Por cada espacio, separar por caracteres
		// Calcular puntuaciones y mostrar nombre: puntuacion
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		String entrada;
		Ejercicio8 ej = new Ejercicio8();

		boolean seguir = true;
		while (seguir) {
			try {
	    		//System.out.println("Escribe nombre: y los naipes");
	    		entrada = reader.readLine();
	    		
	    		if (entrada.equals("fin")) {
	    			seguir = false;
				} else {
					ej.procesarLinea(entrada);
				}

	        } catch (Exception e) {
				// TODO: handle exception
	        	System.out.println(e);
			}
		} 
		ej.listarCartasJugadores();
	}

}
