package unidad8.colecciones;

import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		String entrada;
		int n;
		int k;
		int x;
		
		Deque<Integer> cola = new LinkedList<Integer>();
		
		try {
			System.out.println("Escribe N, K, X");
			entrada = sc.nextLine();
			String[] cadena = entrada.split(" ");
			n = Integer.parseInt(cadena[0]);
			k = Integer.parseInt(cadena[1]);
			x = Integer.parseInt(cadena[2]);
			System.out.println("N: " + n + " K: " + k + " X: " + x);
			
			
			System.out.println("Escribe " + n + " numeros entre los que este el numero " + x);
			entrada = sc.nextLine();
			String[] cadena2 = entrada.split(" ");
			
			for (int i = 0; i < cadena2.length; i++) {
				cola.add(Integer.parseInt(cadena2[i]));
			}
			
			for (int i = 0; i < k; i++) {
				//System.out.println("Eliminado " + cola.getFirst());
				cola.removeFirst();
			}
			
			if (cola.contains(x)) {
				System.out.println(true);
			} else if (cola.peek() == null) {
				System.out.println(0);
			} else {
				int minimo = -1;
				for (Integer num : cola) {
					if (minimo == -1)
						minimo = num;
					else if (num < minimo) {
						minimo = num;
					}
				}
				System.out.println(minimo);
			}	
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

	}

}
