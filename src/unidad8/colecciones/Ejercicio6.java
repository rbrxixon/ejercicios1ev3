package unidad8.colecciones;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

public class Ejercicio6 {
	
	String[] lineaEntrada;
	String comando;
	String entrada;
	String cadena;
	List<String> argumentos = null;
	boolean seguir = true;
	SortedMap<String, Set<String>> listadoContactos;

	private SortedMap<String, Set<String>> getListadoContactos() {
		return listadoContactos;
	}

	private void setListadoContactos(SortedMap<String, Set<String>> listadoContactos) {
		this.listadoContactos = listadoContactos;
	}

	private boolean comprobarCaracter(CharSequence c) throws Exception {
		if (entrada.contains(c)) {
			return true;
		} else {
			return false;
		}
	}
	
	private void comandoConCaracter () throws Exception {
		// Funcion para definir el comando y caracter cuando tiene :
		lineaEntrada = entrada.trim().split(":");
		//System.out.println("definirComandoArgumentosCaracter. lineaEntrada.length = " + lineaEntrada.length);
		comando = lineaEntrada[0].trim();
	}
	
	private void comandoSinCaracter () throws Exception {
		// Funcion para definir el comando y caracter cuando no tiene :
		//System.out.println("definirComandoArgumentosSinCaracter");
		lineaEntrada = entrada.trim().split(" ");
		comando = lineaEntrada[0].trim();	
		if (lineaEntrada.length > 1) {
			throw new Exception("No debes indicar ningun argumento o escribir ':' despues del comando si es necesario");
		}
	}
	
	private void definirArgumentos() throws Exception {
		if (lineaEntrada.length == 2) {
			cadena = lineaEntrada[1];
			argumentos = new ArrayList<String>(Arrays.asList(cadena.trim().split(" ")));
		} else if (lineaEntrada.length == 1) {
			throw new Exception("Si indicas el caracter ':' debes escribir al menos un argumento");
		} else {
			throw new Exception("Solo debes escribir una vez el caracter ':' ");
		}
		//System.out.println("Comando: " + comando);
		//for (String item : argumentos) {
		//	System.out.println("Argumento: " + item);
		//}
	}

	private boolean comprobarArgs(boolean argumentoMultiple) throws Exception {	
		//System.out.println("comprobarArgs. argumentoMultiple: " + argumentoMultiple + " Argumentos size: " + argumentos.size());
		if (argumentoMultiple) {
			if (argumentos.size() < 1) {
				throw new Exception("Debes indicar al menos 1 argumento");
			}
		} else {
			if (argumentos.size() > 1 ) {
				throw new Exception("Debes indicar solo 1 argumento");
			}
		} 
		return true;
	}
	
	private boolean comprobarTelefono() throws Exception {
		for (String telefono : argumentos) {
			if (!(telefono.matches("\\d+"))) {
				throw new Exception("El telefono debe contener unicamente digitos");
			}
		}
		return true;
	}
	
	private void switchOrdenes() throws Exception {
		switch (comando.toLowerCase()) {
		case "buscar":
			if (comprobarCaracter(":")) {
				comprobarArgs(false);
				//System.out.println("Orden: Buscar " + argumentos.get(0));
				listarContacto(argumentos.get(0));
			} else {
				throw new Exception("Para este comando debes escribir el caracter ':'");
			}
			break;
		case "eliminar":
			if (comprobarCaracter(":")) {
				comprobarArgs(false);
				//System.out.println("Orden: Eliminar");
				ordenEliminarContacto(argumentos.get(0));
			} else {
				throw new Exception("Para este comando debes escribir el caracter ':'");
			}
			break;
		case "contactos":
			if (comprobarCaracter(":")) {
				throw new Exception("Para este comando no debes escribir el caracter ':'");
			} else {
				//System.out.println("Orden: Contactos");
				ordenListarContactos();
			}
			break;
		case "salir":
			if (comprobarCaracter(":")) {
				throw new Exception("Para este comando no debes escribir el caracter ':'");
			} else {
				//System.out.println("Orden: Salir");
				seguir = false;
			}
			break;
		default:
			if (comprobarCaracter(":")) {
				comprobarArgs(false);
				//System.out.println("Orden: Insertar contacto");	
				ordenInsertarContacto();
			} else {
				throw new Exception("Comando desconocido");
			}
		}
	}
	
	private void ordenEliminarContacto(String nombre) throws Exception {
		if (getListadoContactos().containsKey(nombre)) {
			//System.out.println("## Contacto: " + nombre + " ##");
			
			Iterator<String> i = getListadoContactos().get(nombre).iterator();
			getListadoContactos().remove(nombre);
	
		} else {
			throw new Exception("No existe el contacto");
		}
	}
	
	private void ordenInsertarContacto() throws Exception {
		comprobarTelefono();
		if (getListadoContactos().containsKey(comando)) {
			//System.out.println("Contacto " + comando + " existente");
			//System.out.println("Contacto" + getListadoContactos().get(comando));
			Set<String> telefonos = getListadoContactos().get(comando);
			
			for (String telefono : telefonos) {
			//for (String telefono : getListadoContactos().get(comando)) {
				//System.out.println("Telefono: " + telefono);
				if (telefono.equals(argumentos.get(0))) {
					throw new Exception("Telefono " + argumentos.get(0) + " ya registrado previamente");
				} 
				//System.out.println("Registrado nuevo telefono " + argumentos.get(0));
			}
			telefonos.add(argumentos.get(0));
			//listarContactos(comando);
		} else {
			//System.out.println("Contacto nuevo registrado");
			Set<String> telefonos = new TreeSet<String>();
			telefonos.add(argumentos.get(0));
			getListadoContactos().put(comando, telefonos);
			//listarContactos(comando);
		}
	}
	
	private void listarContacto(String nombre) throws Exception {
		if (getListadoContactos().containsKey(nombre)) {
			//System.out.println("## Contacto: " + nombre + " ##");
			
			Iterator<String> i = getListadoContactos().get(nombre).iterator();
			while (i.hasNext()) {
				System.out.print(i.next());
				if (i.hasNext())
					System.out.print(", ");
			}
			System.out.println();		
		} else {
			throw new Exception("No existe el contacto");
		}
	}
	
	private void ordenListarContactos() { 
		Iterator<Entry<String, Set<String>>> i = getListadoContactos().entrySet().iterator();
		while(i.hasNext()) {
			Entry<String, Set<String>> e = i.next();
			System.out.println(e.getKey());
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ejercicio6 ej = new Ejercicio6();
		Scanner sc = new Scanner(System.in);
		ej.setListadoContactos(new TreeMap<String, Set<String>>());

		do {
			try {
				System.out.print("> ");
				ej.entrada = sc.nextLine();
				
				if (ej.comprobarCaracter(":")) {
					ej.comandoConCaracter();	
					ej.definirArgumentos();
				} else {
					ej.comandoSinCaracter();
				}		
				ej.switchOrdenes();
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("[ERROR] " + e.getMessage());
			}			
		} while (ej.seguir);
		sc.close();
	}
}
