package unidad8.colecciones;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Ejercicio1 {
	static ArrayList<String> listaEntrada;
	static ArrayList<String> listaSalida;
	
	private static boolean comprobar(String cadena) {
		for (int j = 0; j < listaSalida.size(); j++) {
			if (cadena.equals(listaSalida.get(j))) {
				System.out.println("Igual. Entrada: " + cadena + " Salida: " + listaSalida.get(j));
				return false;
			} else {
	 			System.out.println("Diferente. Entrada: " + cadena + " Salida: " + listaSalida.get(j));
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	       BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
	        String entrada = null; 
	        int numero = 0;
	        listaEntrada = new ArrayList<String>();
	        listaSalida = new ArrayList<String>();
	        
	        try {
        		System.out.print("Introduce numero de nombres: ");
        		entrada = reader.readLine();
	        	numero = Integer.parseInt(entrada);
	        	
	        	for (int i = 0; i < numero; i++) {
	        		System.out.print("Introduce nombre #" + (i+1) + ": ");
	        		entrada = reader.readLine();
	        		listaEntrada.add(entrada);
				}
	        	
	       
	        	for (int i = 0; i < listaEntrada.size(); i++) {
	        		if (comprobar(listaEntrada.get(i))) {
	        			listaSalida.add(listaEntrada.get(i));
	        		}
				}
	        	
	         	System.out.println("SALIDA: ");
	        	for (String stringSalida : listaSalida) {
	        		System.out.println(stringSalida);
	        	}
	        	
	        } catch (Exception e) {
				// TODO: handle exception
	        	System.out.println(e);
			}

	}

}
