package unidad8.ficheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio3 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		String archivo = "/tmp/file1";
		File file = new File(archivo);
		FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        BufferedReader reader = new BufferedReader(input);
        String line;
        
		int countWord = 0;
        int characterCount = 0;
        int lines = 0;
        
     // Reading line by line from the 
        // file until a null is returned
        try {
			while((line = reader.readLine()) != null) {
				lines++;
				characterCount += line.length();
			          
		        // \\s+ is the space delimiter in java
		        String[] wordList = line.split("\\s+");
		        countWord += wordList.length;
			    
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          
        System.out.println("numero de palabras " + countWord);
		System.out.println("numero de caracteres " + characterCount);
		System.out.println("numero de lineas " + lines);

	}

}


