package unidad8.ficheros;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class Ejercicio2 {
	FileInputStream fis;
	String archivo = "/home/rober/Descargas/El Quijote UTF-8.txt";	
	File f = new File(archivo);
	BufferedInputStream bis = null;
	
	private void sinBuffer() throws IOException {
		fis = new FileInputStream(archivo);		
		int content;
        while ((content=fis.read())!=-1){
            System.out.print((char) content);
        }
        fis.close();
	}
	
	private void conBuffer() throws IOException {
        fis = new FileInputStream(f);		
        bis = new BufferedInputStream(fis);
        while( bis.available() > 0 ){             	
              System.out.print((char)bis.read());
          }	
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ejercicio2 e = new Ejercicio2();
		try {
			e.sinBuffer();
			e.conBuffer();
		} catch (Exception ex) {
			// TODO: handle exception
			System.out.println(ex);
		}
			
	}

}
