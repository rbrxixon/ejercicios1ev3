package unidad8.ficheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import unidad8.colecciones.Palabras;

public class Ejercicio7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 try {
			 String fileName = "/home/rober/Descargas/El Quijote UTF-8.txt";
			 File f = new File(fileName);
			 String cadena = null;
			 InputStream is = new FileInputStream(fileName);
			 BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			 String str;
			    while ((str = br.readLine()) != null) {
			    	cadena += str;
			    	//System.out.println(str);
			    }
			    System.out.println(cadena);
			    Palabras p = new Palabras(cadena);
		 	} catch (Exception e) {
				// TODO: handle exception
		 		System.out.println(e);
			}
	}
	

}
