package unidad8.ficheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio4 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		String archivo = "/tmp/file1";
		String archivo2 = "/tmp/file1_data";
		File file = new File(archivo);
		File file_data = new File(archivo2);
		FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        BufferedReader reader = new BufferedReader(input);
        String line;
        FileOutputStream fop = null;
        
		int countWord = 0;
        int characterCount = 0;
        int lines = 0;
        byte[] contentInBytes;
        String content;
        
     // Reading line by line from the 
        // file until a null is returned
        try {
			while((line = reader.readLine()) != null) {
				lines++;
				characterCount += line.length();
			          
		        // \\s+ is the space delimiter in java
		        String[] wordList = line.split("\\s+");
		        countWord += wordList.length;
			}
			
			fop = new FileOutputStream(file_data, true);
			 // if file doesnt exists, then create it
            if (!file_data.exists()) {
            	file_data.createNewFile();
            }
            
            content = "numero de palabras: " + countWord + "\n" + "numero de caracteres: " + characterCount + "\n" + "numero de lineas: " + lines + "\n";
            		
            // get the content in bytes
            contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          

	}

}
