package unidad8.ficheros;

import java.io.File;

public class Ejercicio1 {
	
	private static void leerFS(String ruta) {
		File f = new File(ruta);
		
		System.out.println("Existe? " + f.exists());
		
		System.out.println("Si se trata de un fichero o de un directorio");
		System.out.println("Es directorio?" + f.isDirectory() + " o es archivo? " + f.isFile());
		
		System.out.println("Nombre del fichero o directorio " + f.getName());
		System.out.println("permisos de acceso. Leer " + f.canRead() + " Ejecucion " + f.canExecute() + " Escribir " + f.canWrite());
		
		System.out.println("Si se trata de un fichero, debe mostrar además el tamaño del mismo");
		if (f.isFile())
			System.out.println(f.length());
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String fs = "/tmp/file1";
			leerFS(fs);			
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
